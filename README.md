
## Cara Install ( inggris biar pinter )


1. Using your editor, duplicate `web/config/db-local.php` to be `web/config/db.php` and add your database credentials credentials. If you are using docker as an local environment, then just rename `web/config/db-docker.php` to be `web/config/db.php`
2. Start the docker `sh scripts/docker.sh up`
2. Connect to the docker `php-fpm` using the following command: `docker exec -it material-dashboard-php-fpm /bin/bash`
3. Run `composer install` to install the vendor
4. After Composer is finished, run migration command `php yii migrate/up` and type `Y`

## Usage

A user will be already created for you to login from the migration command `hello@coderseden.com` with password `admin123` so you can login and browse.

Besides the dashboard and the auth pages this preset also has a full CRUD example on users management (Create User, View User, Update User, Delete User). All the necessary files (controllers, models, views) are installed out of the box and all the needed routes are added to `web/config/web.php`. Keep in mind that all of the features can be viewed once you login using the credentials provided above. 

### Dashboard

You can access the dashboard either by using the "**Dashboard**" link in the left sidebar. 


## Documentation


## File Structure
```
├── controllers
│   ├── DashboardController.php
│   ├── IconsController.php
│   ├── MapController.php
│   ├── NotificationsController.php
│   ├── SiteController.php
│   ├── TablesController.php
│   ├── TypographyController.php
│   └── UsersController.php
├── migrations
│   └── m200216_223841_create_user_table.php
├── models
│   ├── User.php
│   ├── UserForgotForm.php
│   ├── UserLoginForm.php
│   └── UserSearch.php
├── views
│   ├── dashboard
│   │   └── index.php
│   ├── icons
│   │   └── index.php
│   ├── layouts
│   │   ├── admin.php
│   │   ├── content.php
│   │   ├── footer.php
│   │   ├── header.php
│   │   ├── left.php
│   │   ├── main.php
│   │   └── plugin.php
│   ├── map
│   │   └── index.php
│   ├── notifications
│   │   └── index.php
│   ├── site
│   │   ├── error.php
│   │   ├── forgot.php
│   │   └── index.php
│   ├── tables
│   │   └── index.php
│   ├── typography
│   │   └── index.php
│   └── users
│       ├── form.php
│       ├── list.php
│       └── view.php
├── web
│   ├── css
│   │   ├── material-dashboard-rtl.css
│   │   ├── material-dashboard.css
│   │   ├── material-dashboard.css.map
│   │   └── material-dashboard.min.css
│   ├── demo
│   │   ├── demo.css
│   │   └── demo.js
│   ├── img
│   │   ├── apple-icon.png
│   │   ├── bg2.jpg
│   │   ├── city-profile.jpg
│   │   ├── city.jpg
│   │   ├── cover.jpg
│   │   ├── faces
│   │   │   ├── avatar.jpg
│   │   │   ├── card-profile1-square.jpg
│   │   │   ├── card-profile2-square.jpg
│   │   │   └── marc.jpg
│   │   ├── favicon.png
│   │   ├── login.jpg
│   │   ├── mask.png
│   │   ├── new_logo.png
│   │   ├── sidebar-1.jpg
│   │   ├── sidebar-2.jpg
│   │   ├── sidebar-3.jpg
│   │   ├── sidebar-4.jpg
│   │   └── yii-logo.svg
│   ├── js
│   │   ├── core
│   │   │   ├── bootstrap-material-design.min.js
│   │   │   ├── jquery.min.js
│   │   │   └── popper.min.js
│   │   ├── material-dashboard.js
│   │   ├── material-dashboard.js.map
│   │   ├── material-dashboard.min.js
│   │   ├── plugin.js
│   │   └── plugins
│   │       ├── arrive.min.js
│   │       ├── bootstrap-datetimepicker.min.js
│   │       ├── bootstrap-notify.js
│   │       ├── bootstrap-selectpicker.js
│   │       ├── bootstrap-tagsinput.js
│   │       ├── chartist.min.js
│   │       ├── fullcalendar.min.js
│   │       ├── jasny-bootstrap.min.js
│   │       ├── jquery-jvectormap.js
│   │       ├── jquery.bootstrap-wizard.js
│   │       ├── jquery.dataTables.min.js
│   │       ├── jquery.tagsinput.js
│   │       ├── jquery.validate.min.js
│   │       ├── moment.min.js
│   │       ├── nouislider.min.js
│   │       ├── perfect-scrollbar.jquery.min.js
│   │       └── sweetalert2.js
│   └── scss
│       ├── material-dashboard
│       │   ├── _alerts.scss
│       │   ├── _buttons.scss
│       │   ├── _cards.scss
│       │   ├── _checkboxes.scss
│       │   ├── _core-bootstrap.scss
│       │   ├── _dropdown.scss
│       │   ├── _example-pages.scss
│       │   ├── _fixed-plugin.scss
│       │   ├── _footers.scss
│       │   ├── _forms.scss
│       │   ├── _headers.scss
│       │   ├── _images.scss
│       │   ├── _info-areas.scss
│       │   ├── _input-group.scss
│       │   ├── _misc.scss
│       │   ├── _mixins.scss
│       │   ├── _navbar.scss
│       │   ├── _popover.scss
│       │   ├── _popups.scss
│       │   ├── _radios.scss
│       │   ├── _responsive.scss
│       │   ├── _ripples.scss
│       │   ├── _sidebar-and-main-panel.scss
│       │   ├── _social-buttons.scss
│       │   ├── _tables.scss
│       │   ├── _tabs.scss
│       │   ├── _togglebutton.scss
│       │   ├── _tooltip.scss
│       │   ├── _type.scss
│       │   ├── _variables.scss
│       │   ├── bootstrap
│       │   │   └── scss
│       │   │       ├── _alert.scss
│       │   │       ├── _badge.scss
│       │   │       ├── _breadcrumb.scss
│       │   │       ├── _button-group.scss
│       │   │       ├── _buttons.scss
│       │   │       ├── _card.scss
│       │   │       ├── _carousel.scss
│       │   │       ├── _close.scss
│       │   │       ├── _code.scss
│       │   │       ├── _custom-forms.scss
│       │   │       ├── _dropdown.scss
│       │   │       ├── _forms.scss
│       │   │       ├── _functions.scss
│       │   │       ├── _grid.scss
│       │   │       ├── _images.scss
│       │   │       ├── _input-group.scss
│       │   │       ├── _jumbotron.scss
│       │   │       ├── _list-group.scss
│       │   │       ├── _media.scss
│       │   │       ├── _mixins.scss
│       │   │       ├── _modal.scss
│       │   │       ├── _nav.scss
│       │   │       ├── _navbar.scss
│       │   │       ├── _pagination.scss
│       │   │       ├── _popover.scss
│       │   │       ├── _print.scss
│       │   │       ├── _progress.scss
│       │   │       ├── _reboot.scss
│       │   │       ├── _root.scss
│       │   │       ├── _tables.scss
│       │   │       ├── _tooltip.scss
│       │   │       ├── _transitions.scss
│       │   │       ├── _type.scss
│       │   │       ├── _utilities.scss
│       │   │       ├── _variables.scss
│       │   │       ├── bootstrap-grid.scss
│       │   │       ├── bootstrap-reboot.scss
│       │   │       ├── bootstrap.scss
│       │   │       ├── mixins
│       │   │       │   ├── _alert.scss
│       │   │       │   ├── _background-variant.scss
│       │   │       │   ├── _badge.scss
│       │   │       │   ├── _border-radius.scss
│       │   │       │   ├── _box-shadow.scss
│       │   │       │   ├── _breakpoints.scss
│       │   │       │   ├── _buttons.scss
│       │   │       │   ├── _caret.scss
│       │   │       │   ├── _clearfix.scss
│       │   │       │   ├── _float.scss
│       │   │       │   ├── _forms.scss
│       │   │       │   ├── _gradients.scss
│       │   │       │   ├── _grid-framework.scss
│       │   │       │   ├── _grid.scss
│       │   │       │   ├── _hover.scss
│       │   │       │   ├── _image.scss
│       │   │       │   ├── _list-group.scss
│       │   │       │   ├── _lists.scss
│       │   │       │   ├── _nav-divider.scss
│       │   │       │   ├── _navbar-align.scss
│       │   │       │   ├── _pagination.scss
│       │   │       │   ├── _reset-text.scss
│       │   │       │   ├── _resize.scss
│       │   │       │   ├── _screen-reader.scss
│       │   │       │   ├── _size.scss
│       │   │       │   ├── _table-row.scss
│       │   │       │   ├── _text-emphasis.scss
│       │   │       │   ├── _text-hide.scss
│       │   │       │   ├── _text-truncate.scss
│       │   │       │   ├── _transition.scss
│       │   │       │   └── _visibility.scss
│       │   │       └── utilities
│       │   │           ├── _align.scss
│       │   │           ├── _background.scss
│       │   │           ├── _borders.scss
│       │   │           ├── _clearfix.scss
│       │   │           ├── _display.scss
│       │   │           ├── _embed.scss
│       │   │           ├── _flex.scss
│       │   │           ├── _float.scss
│       │   │           ├── _position.scss
│       │   │           ├── _screenreaders.scss
│       │   │           ├── _sizing.scss
│       │   │           ├── _spacing.scss
│       │   │           ├── _text.scss
│       │   │           └── _visibility.scss
│       │   ├── cards
│       │   │   ├── _card-plain.scss
│       │   │   ├── _card-profile.scss
│       │   │   └── _card-stats.scss
│       │   ├── mixins
│       │   │   ├── _alert.scss
│       │   │   ├── _animations.scss
│       │   │   ├── _breakpoints.scss
│       │   │   ├── _buttons.scss
│       │   │   ├── _chartist.scss
│       │   │   ├── _colored-shadows.scss
│       │   │   ├── _drawer.scss
│       │   │   ├── _forms.scss
│       │   │   ├── _hover.scss
│       │   │   ├── _layout.scss
│       │   │   ├── _navbar-colors.scss
│       │   │   ├── _navs.scss
│       │   │   ├── _sidebar-color.scss
│       │   │   ├── _transparency.scss
│       │   │   ├── _type.scss
│       │   │   ├── _utilities.scss
│       │   │   ├── _variables.scss
│       │   │   └── _vendor-prefixes.scss
│       │   ├── plugins
│       │   │   ├── _animate.scss
│       │   │   ├── _chartist.scss
│       │   │   └── _perfect-scrollbar.scss
│       │   └── variables
│       │       ├── _body.scss
│       │       ├── _bootstrap-material-design-base.scss
│       │       ├── _bootstrap-material-design.scss
│       │       ├── _brand.scss
│       │       ├── _buttons.scss
│       │       ├── _card.scss
│       │       ├── _code.scss
│       │       ├── _colors-map.scss
│       │       ├── _colors.scss
│       │       ├── _custom-forms.scss
│       │       ├── _drawer.scss
│       │       ├── _dropdown.scss
│       │       ├── _forms.scss
│       │       ├── _layout.scss
│       │       ├── _list-group.scss
│       │       ├── _menu.scss
│       │       ├── _modals.scss
│       │       ├── _nav.scss
│       │       ├── _pagination.scss
│       │       ├── _shadow.scss
│       │       ├── _snackbar.scss
│       │       ├── _spacing.scss
│       │       ├── _state.scss
│       │       ├── _tables.scss
│       │       ├── _tooltip.scss
│       │       └── _type.scss
│       └── material-dashboard.scss
```

## Browser Support

At present, we officially aim to support the last two versions of the following browsers:

<img src="https://github.com/creativetimofficial/public-assets/blob/master/logos/chrome-logo.png?raw=true" width="64" height="64"> <img src="https://raw.githubusercontent.com/creativetimofficial/public-assets/master/logos/firefox-logo.png" width="64" height="64"> <img src="https://raw.githubusercontent.com/creativetimofficial/public-assets/master/logos/edge-logo.png" width="64" height="64"> <img src="https://raw.githubusercontent.com/creativetimofficial/public-assets/master/logos/safari-logo.png" width="64" height="64"> <img src="https://raw.githubusercontent.com/creativetimofficial/public-assets/master/logos/opera-logo.png" width="64" height="64">
